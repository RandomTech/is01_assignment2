#include <stdio.h>
int main(){
	char ltr;
 
  printf("Enter a character\n");
  scanf("%c", &ltr);

  if (ltr == 'a' || ltr == 'A' || ltr == 'e' || ltr == 'E' || ltr== 'i' || ltr == 'I' || ltr =='o' || ltr=='O' || ltr == 'u' || ltr == 'U')
    printf("%c is a Vowel.\n", ltr);
  else
    printf("%c is a Consonant.\n", ltr);
     
  return 0;
}
